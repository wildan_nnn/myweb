@extends('layout.layout')

@section('title')
	Parser
@endsection

@section('content')
<h2>This</h2>
@if ($message = Session::get('success'))
  	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	  	<strong>{{ $message }}</strong>
  	</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-info">
			<div class="panel-heading"><h2>Parser</h2></div>
			<div class="panel-body">
				<table class="table table-striped table-hover" id="dataTables">
					<thead>
						<tr>
							<td>No</td>
							<td class="col-md-4">Awal</td>
							<td>Isi</td>
							<td class="col-md-2 text-center">Tools</td>
						</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;
					?>
					@foreach($parser as $pars)
						<tr>
							<td>{{ $pars->id }}</td>
							<td>{{ $pars->tanya }}</td>
							<td>{{ $pars->jawab }}</td>
							<td class="text-center">
								<form action="{{ route('delete',['id' => $pars->id]) }}" method="post">
									{{ csrf_field() }}
									<a href="{{ url('parser/crud/edit/'.$pars->id) }}" class="btn btn-warning"><span class="fa fa-pencil"></span> </a><button class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus?')"><span class="fa fa-trash"></span> </button>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="pull-right">{{ $parser->links() }}</div>
			</div>
		</div>
	</div>
</div>
@endsection