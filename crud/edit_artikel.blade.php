@extends('layout.layout')

@section('title')
	Edit Artikel
@endsection

@section('content')
<h2>Edit</h2>
<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" action="{{ url('artikel/update/'.$b->id_artikel) }}" method="post">
			{{ csrf_field() }}
			<table class="table table-striped">
				<tr>
					<td>Judul</td>
					<td><input type="text" name="judul" class="form-control" value="{{ $b->judul }}"></td>
				</tr>
				<tr>
					<td>Isi</td>
					<td><textarea name="isi" class="form-control" required>{{ $b->isi }}</textarea></td>
				</tr>
				<tr>
					<td>Siapa</td>
					<td>
						<select name="join" class="form-control">
							<option value="$b->id">{{ $b->name }}</option>
							<option value="">&mdash; Pilih Dibawah</option>
							@foreach($user as $pengguna)
							<option value="{{ $pengguna->id }}">{{ $pengguna->name }}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="submit" class="btn btn-primary" value="Simpan"><a href="{{ url('artikel/v_artikel') }}" class="btn btn-warning">Kembali</a></td>
				</tr>
			</table>
		</form>
	</div>
</div>
@endsection