@extends('layout.layout')

@section('title')
	Tambah Artikel
@endsection

@section('content')
<h2>Tambah</h2>
<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" action="{{ url('artikel/simpan') }}" method="post">
			{{ csrf_field() }}
			<table class="table table-striped">
				<tr>
					<td>Judul</td>
					<td><input type="text" name="judul" class="form-control" required></td>
				</tr>
				<tr>
					<td>Isi</td>
					<td><textarea name="isi" class="form-control" required></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="submit" class="btn btn-primary" value="Simpan"><a href="{{ url('artikel/v_artikel') }}" class="btn btn-warning">Kembali</a></td>
				</tr>
			</table>
		</form>
	</div>
</div>
@endsection